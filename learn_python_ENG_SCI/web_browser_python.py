import webbrowser

f = open('helloworld.html','w')

message = """<!DOCTYPE html>
<html>

<body>

<style >
div#map { margin: 0 0 0 720px; }

table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 50%;
   
    margin: -500px 0 0 0;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
    
</style>

<div id="map"  style="width:50%;height:500px"></div>


<script>
function myMap() {
  var myCenter = new google.maps.LatLng(51.508742,-0.120850);
  var myCenter1 = new google.maps.LatLng(53.508742,-0.150850);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 5, mapTypeId: google.maps.MapTypeId.HYBRID};
 
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  var marker1 = new google.maps.Marker({position:myCenter1});
  marker.setMap(map);
  marker1.setMap(map);
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>



<table>
  <tr>
    <th>ID</th>
    <th>Property Description</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td>Mexico</td>
  </tr>
  <tr>
    <td>Ernst Handel</td>
    <td>Roland Mendel</td>
    <td>Austria</td>
  </tr>
  <tr>
    <td>Island Trading</td>
    <td>Helen Bennett</td>
    <td>UK</td>
  </tr>
  <tr>
    <td>Laughing Bacchus Winecellars</td>
    <td>Yoshi Tannamuri</td>
    <td>Canada</td>
  </tr>
  <tr>
    <td>Magazzini Alimentari Riuniti</td>
    <td>Giovanni Rovelli</td>
    <td>Italy</td>
  </tr>
</table>


</body>
</html>
"""

f.write(message)
f.close()
#var mapOptions3 = {
#    center: new google.maps.LatLng(51.508742,-0.120850),
#    zoom:9,
#    mapTypeId: google.maps.MapTypeId.HYBRID
#  };
#var mapOptions {disableDefaultUI: true}

webbrowser.open_new_tab('helloworld.html')