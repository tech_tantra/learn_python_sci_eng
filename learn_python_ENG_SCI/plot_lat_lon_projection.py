#!/usr/bin/env python

#code to get basemap and plot the lat lon over it
#

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np

def plot_map(lat, lon):
    fig = plt.figure(figsize=(14,8))
    eq_map = Basemap(resolution = 'l', area_thresh = 10000.0,)
            
    eq_map.drawcoastlines()
    eq_map.drawcountries()
     #eq_map.bluemarble()
    #eq_map.etopo()
    eq_map.shadedrelief()
    eq_map.drawmapboundary()
    
    x,y = eq_map(lon, lat)
    eq_map.plot(x,y, 'v',color='Red',markersize=10)
    plt.title("Latitude and Longitude ploted over the Earth projection map")
    #fname = 'C:\\Users\\sbhatta\Desktop\\w_plots\\worldMap.jpeg'
    #plt.savefig(fname, dpi=100, bbox_inches='tight')
    
def main():
    lat = [20.1, 22.2, 23.1]
    lon = [24.1, 21.2, 25.2]
    
    plot_map(lat, lon)
    
if __name__ =='__main__':
    main()