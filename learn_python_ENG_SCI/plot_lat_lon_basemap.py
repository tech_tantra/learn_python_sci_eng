#!/usr/bin/env python

#code to get basemap and plot the lat lon over it
#

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np

def plot_map(lat, lon):
    fig = plt.figure(figsize=(14,8))
    m = Basemap(resolution = 'l', area_thresh = 10000.0,)
            
    m.drawcoastlines()
    m.drawcountries()
     #eq_map.bluemarble()
    #eq_map.etopo()
    m.shadedrelief()
    m.drawmapboundary()
    
    x,y = eq_map(lon, lat)
    eq_map.plot(x,y, 'v',color='Red',markersize=10)
    plt.title("Latitude and Longitude projected on Earth map")
    filename = 'filename'
    plt.savefig(filename, dpi=100, bbox_inches='tight')
    
def main():
    lat = [35.12, 37.12, 34.31]
    lon = [34.3, 31.21, 35.22]
    
    plot_map(lat, lon)
    
if __name__ =='_
3_main__':
    main()
