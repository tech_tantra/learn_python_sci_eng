#!/usr/bin/env python

#learning python
#
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec

def plot_data():
    #data can be passed as arguments
    #
  
    xdata = np.arange(648.75, 1096.875, 0.625) # x-data
    ydata = np.random.rand(717) #random y data

        
    fig   = plt.figure(figsize = (10,6))
    gs    = gridspec.GridSpec(2,1)
    ax1   = plt.subplot(gs[0,0])
    ax2   = plt.subplot(gs[1,0])
  
    ax1.plot(xdata, ydata, '.', color = 'r', markersize = 2, clip_on=False)
    #ax1.semilogy() #can be used to plot log of the data
    ax1.set_title('Random data plot A', fontsize = 8)
    ax1.set_xlabel('x axis')
    ax1.set_ylabel('y -axis')
    #ax1.setylim([-1.5])
    ax1.grid()
        
    ax2.plot(xdata, ydata, '.', color = 'b', markersize = 2, clip_on=False)
    ax2.set_title('Random data plot B', fontsize = 8)
    ax2.set_xlabel('x axis')
    ax2.set_ylabel('y -axis')
    #ax2.set_yscale('log') #can be used to plot log of the data
    ax2.grid()
        
        
    plt.suptitle('Plot using python ', y=1.05)
    plt.tight_layout(pad =0.4, w_pad=0.5, h_pad=0.90)
    
    #saving the plots
    #
    filename1= "pathAndnameofFigure.jpg"
    fig.savefig(filename1, dpi =100)
    
if __name__=='__main__':
    plot_data()
    