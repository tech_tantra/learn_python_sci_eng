#!/usr/bin/env python

# function to obtain Brightness temperature of object based on Plancks function
# works for to obtain radiance or temperature
# inputs are radiance and wavenumer to get temperature
# inputs are temperature and wavenumber to get radiance

import numpy as np

def planck_bth(radiance, wavenum):
    planck = 6.6260693e-34
    c = 2.99792458e8
    boltzman = 1.3806505e-23

    #  now in cm-K
    #
    c2 = planck * c / boltzman * 1.0e2
    #  now in  mW/m^2/sr/cm^4
    #
    c1 = 2.0 * planck * np.power(c,2) * 1.0e11
    
    # to get rad from BT
    #
    #radiance = ( c1 * np.power(wavenum,3)) / ( np.exp(c2 * wavenum / temperature) - 1.0e0)

    #to get BT from rad
    #

    bth = (c2 * wavenum) / ( np.log(  (c1 * np.power(wavenum,3) / radiance) + 1.0e0 ))

    return (bth)