#!/usr/bin/env python

# code to use SQLITE database with python
#

import sqlite3

# Create a database in memory
#
createDb = sqlite3.connect(':memory:')

# Creates the SQlite cursor to query db
#
queryCurs = createDb.cursor()

def createTable():
# create table SQL Query

    queryCurs.execute('''create table students
(id integer primary key, name text, street text, city text, state text, degree text, roll integer, balance real)''')

# id will auto increment according to sql language
#
def addStudent(name, street, city, state, degree, roll, balance):    
# function will call execute method to submit SQL Query
    queryCurs.execute('''insert into students (name, street, city, state, degree, roll, balance) values (?, ?, ?, ?, ?,?,?)''',(name, street, city, state, degree, roll, balance))


def update_name_pay(name,street, roll):
    with createDb:
        queryCurs.execute("""UPDATE students SET roll = :roll WHERE name = :name AND street = :street""",{'name': name, 'street': street, 'roll': roll})


def remove_name_roll(name, roll):
    with createDb:
        queryCurs.execute("DELETE from students WHERE name = :name AND roll = :roll",
                  {'name': name, 'roll': roll})
                  
                  
def main():
# this function will create the table in the database
    createTable()

# Add students to the database
    addStudent('Jon Snow', '6008 Emerson St', 'College Park', 'MD', 'Masters', 15, 150.76)
    addStudent('Bran Stark', '6108 Emerson St', 'College Park', 'MD', 'Masters', 13, 151.76)
    addStudent('Sansa Stark', '6208 Emerson St', 'College Park', 'MD', 'Masters', 12, 152.76)
    addStudent('Arya Stark', '6038 Emerson St',  'College Park', 'MD', 'Masters', 15, 102.76)
    
    update_name_pay( 'Jon Snow', '6008 Emerson St', 20)
    
    #remove_name_roll('Sansa Stark', 12)

# make changes by commit command
#
    createDb.commit()

    queryCurs.execute('select * from students')

# loop the tuple and prints the entries 
#    for i in queryCurs:
#        print '\n'
#        for j in i:
#            print j

# Print customers ordered by lowest balance and with titles
    queryCurs.execute('select * from students order by roll')

# Creates a list that contains the titles for my database data
#
    listTitle = ['Id Num ','Name ','Street ','City ','State ', 'Degree', 'Roll', 'Balance ']
    k = 0

# loop through data and prints the entries 
#
    for i in queryCurs:
        print '\n'
        for j in i:
            print listTitle[k],
            print j
            if k < 7: k += 1
            else: k = 0

# Closes the database
#
    queryCurs.close()

if __name__ == '__main__': main()
