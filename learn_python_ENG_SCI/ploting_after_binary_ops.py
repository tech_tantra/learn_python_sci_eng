#dssymmetry v2 similar to rayatheon
#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('svg')
import matplotlib.pyplot as plt
from matplotlib import gridspec


def qf4_flag(data):

    dataArray = np.array(data)
    print "shape of the ds spectra:", np.shape(dataArray)
    
    gx = [0, 1, 2, 0, 1, 2, 0, 1, 2]
    gy = [0, 0, 0, 1, 1, 1, 2, 2, 2]
    binaryMask = [0b00000001, 0b00000010, 0b00000100, 0b00001000, 0b00010000]
    bitShift = [0, 1, 2, 3, 4]
    k=0
    titleString = ["Long-Wave-CrIS-SDR-QF4-Day-Night-Indicator", "Mid-Wave-CrIS-QF4-Day-Night-Indicator", "Short-Wave-CrIS-QF4-Day-Night-Indicator",\
    "Long-Wave-CrIS-SDR-QF4-Invalid-RDR-Data", "Mid-Wave-CrIS-QF4-Invalid-RDR-Data", "Short-Wave-CrIS-QF4-Invalid-RDR-Data",\
    "Long-Wave-CrIS-SDR-QF4-Fringe-Count-Err-Detection", "Mid-Wave-CrIS-QF4-Fringe-Count-Err-Detection", "Short-Wave-CrIS-QF4-Fringe-Count-Err-Detection",\
    "Long-Wave-CrIS-SDR-QF4-Bit-Trim-Failed", "Mid-Wave-CrIS-QF4-Bit-Trim-Failed", "Short-Wave-CrIS-QF4-Bit-Trim-Failed",\
    "Long-Wave-CrIS-SDR-QF4-Img-part-Radiance-Anamoly", "Mid-Wave-CrIS-QF4-Img-part-Radiance-Anamoly", "Short-Wave-CrIS-QF4-Img-part-Radiance-Anamoly"]
    
    
    for m in range(5):
        im = dict()
    
        for b in range(3):
            gs = gridspec.GridSpec(3,3)    
            fig = plt.figure(figsize = (15,8))
            fig.canvas.draw()
    #seperate data based on bands
    #
            data = dataArray[:, :, :, :, b]
            
        
            for f in range(9):
                    fovData = data[:, :, :, f]
                    
    # seperate data based on fov
    #
                    es= dict()
  
                    for e in range(30):
                        es[e] = np.bitwise_and(fovData[:, :, e], binaryMask[m]).ravel()>>bitShift[m]
    #all data in one Array for each band
    #
                    bandData = np.vstack((es[0], es[1], es[2], es[3], es[4], es[5], es[6], es[7], es[8], es[9], es[10], es[11], es[12], es[13], es[14], es[15], es[16], es[17],es[18], es[19], es[20], es[21], es[22], es[23], es[24], es[25], es[26],es[27],es[28], es[29])) 
 
    #ploting
            
                    ax1 = plt.subplot(gs[gx[f],gy[f]])
                    major_yticks = np.arange(5,30,5)
                    im = ax1.imshow(bandData, cmap = 'jet', aspect ='auto', interpolation ='none' )
                    ax1.set_title(titleString[k] + " FOV "+ str(f+1) , fontsize =8)
                    ax1.set_ylabel('ES #')
                    ax1.set_xlabel('SCAN #')
                    ax1.set_yticks(major_yticks)
    #make colorbar values
    #
                    m0=(bandData.min())                  # colorbar min value
                    m4=(bandData.max())                  # colorbar max value
                    m1=1*(m4-m0)/4.0 + m0                # colorbar mid value 1
                    m2=2*(m4-m0)/4.0 + m0                # colorbar mid value 2
                    m3=3*(m4-m0)/4.0 + m0                # colorbar mid value 3
                    plt.colorbar(im, ax = ax1, ticks=[m0, m1, m2, m3, m4],  pad=0.03)
            
    
            plt.suptitle(str(titleString[k])+ "  QF4_CRISSDR" + ", Date ", y=1.05)
            plt.tight_layout(pad =0.4, w_pad=0.5, h_pad=0.90)

    #save the plot
            filename= 'C:\\Users\\sbhatta\\Desktop\\w_plots\\plots1\\' +str(titleString[k])+'.jpg'
            k = k+1
    
            plt.savefig(filename, dpi =100, bbox_inches='tight')
    
   
    
    print "done"
    
    